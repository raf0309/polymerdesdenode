# Imagen base
FROM node:latest

# Direcctorio de la app
WORKDIR /app

# Copio archivos realese
ADD /build/default /app/build/default
# Copio archivos node
ADD server.js /app
ADD package.json /app

# Dependencias
RUN npm install

# Puerto que expongo
EXPOSE 3000

#Comando
CMD ["npm","start"]
